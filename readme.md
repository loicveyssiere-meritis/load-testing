# Load Testing Setup

## Structure

- `app`: contains a basic JAVA Web application with different endpoints to test
- `deploy`: contains Azure scripts to deploy different cloud infrastructures
- `tests`: contains load-testing tools based on grafana K6
