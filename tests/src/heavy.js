import http from 'k6/http';
import { check, sleep } from 'k6';

const domain = __ENV.DOMAIN_NAME;
const pool = __ENV.POOL

export const options = {
  vus: 10,
  duration: '120s'
};


export default function () {
  const res = http.get(`http://${pool}.${domain}/api/v1/heavy/`);
  check(res, { 'status was 200': (r) => r.status == 200 });
  sleep(10);
}