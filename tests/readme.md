# Load Testing

## Setup

Run tools

```bash
docker compose up grafana influxdb
```

Run tests

```bash
docker compose run --rm k6 run /scripts/fake.js
```