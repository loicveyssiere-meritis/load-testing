package fr.love.load;

import fr.love.load.server.Configuration;
import fr.love.load.server.WebServer;

public class WebApplication {

    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        WebServer server = new WebServer(configuration);
        server.startServer();
    }

}
