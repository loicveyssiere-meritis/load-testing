package fr.love.load.server;

@FunctionalInterface
public interface Controller {
    void routes();
}
