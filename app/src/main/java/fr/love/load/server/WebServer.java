package fr.love.load.server;

import fr.love.load.modules.base.BaseController;
import fr.love.load.error.GenericException;
import fr.love.load.modules.payload.PayloadController;
import fr.love.load.modules.polling.PollingController;
import io.javalin.Javalin;
import io.javalin.config.JavalinConfig;
import io.javalin.openapi.plugin.OpenApiConfiguration;
import io.javalin.openapi.plugin.OpenApiPlugin;
import io.javalin.openapi.plugin.redoc.ReDocConfiguration;
import io.javalin.openapi.plugin.redoc.ReDocPlugin;
import io.javalin.openapi.plugin.swagger.SwaggerConfiguration;
import io.javalin.openapi.plugin.swagger.SwaggerPlugin;
import io.javalin.validation.JavalinValidation;
import io.javalin.validation.ValidationException;

import java.util.ArrayList;
import java.util.UUID;

import static io.javalin.apibuilder.ApiBuilder.path;

public class WebServer {

    private final Configuration configuration;

    public WebServer(Configuration configuration) {
        this.configuration = configuration;
    }

    public void startServer() {
        Javalin app = Javalin.create(config -> {
            config.http.maxRequestSize = 2000000L;
            swaggerConfiguration(config);
        });
        // Error handling
        app.exception(ValidationException.class, new GenericException());
        JavalinValidation.register(UUID.class, UUID::fromString);

        // Controllers
        app.routes(() -> path("/api/v1", () -> controllers().forEach(Controller::routes)));

        //  Start app
        app.start(configuration.applicationPort());
    }

    private ArrayList<Controller> controllers() {
        ArrayList<Controller> _controllers = new ArrayList<>();
        _controllers.add(new BaseController());
        _controllers.add(new PayloadController());
        _controllers.add(new PollingController());
        return _controllers;
    }

    private void swaggerConfiguration(JavalinConfig config) {
        String deprecatedDocsPath = "/swagger-docs";

        OpenApiConfiguration openApiConfiguration = new OpenApiConfiguration();
        openApiConfiguration.getInfo().setTitle("Load Testing");
        openApiConfiguration.setDocumentationPath(deprecatedDocsPath);
        config.plugins.register(new OpenApiPlugin(openApiConfiguration));

        SwaggerConfiguration swaggerConfiguration = new SwaggerConfiguration();
        swaggerConfiguration.setUiPath("/swagger"); // by default it's /swagger
        swaggerConfiguration.setDocumentationPath(deprecatedDocsPath);
        config.plugins.register(new SwaggerPlugin(swaggerConfiguration));

        ReDocConfiguration reDocConfiguration = new ReDocConfiguration();
        reDocConfiguration.setUiPath("/redoc"); // by default it's /redoc
        reDocConfiguration.setDocumentationPath(deprecatedDocsPath);
        config.plugins.register(new ReDocPlugin(reDocConfiguration));
    }
}
