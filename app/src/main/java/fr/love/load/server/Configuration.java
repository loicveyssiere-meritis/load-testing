package fr.love.load.server;

public class Configuration {

    private boolean isNullOrEmpty(String s) {
        return s == null || s.trim().isEmpty();
    }

    private int getPropertyAsInt(String key, int defaults) {
        var value = System.getenv(key);
        if (isNullOrEmpty(value)) {
            return defaults;
        }
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(key + " is not a valid integer");
        }
    }
    public int applicationPort() {
        return getPropertyAsInt("JAVALIN_PORT", 8080);
    }
}