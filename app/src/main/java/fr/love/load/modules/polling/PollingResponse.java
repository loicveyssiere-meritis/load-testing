package fr.love.load.modules.polling;

import io.javalin.openapi.OpenApiByFields;

@OpenApiByFields
public class PollingResponse {
    public String name;
    public String company;
    public String description;
    public String summary;
    public String key;
    public String secret;
    public String info;
}
