package fr.love.load.modules.base;

import fr.love.load.server.Controller;
import io.javalin.http.Context;
import io.javalin.http.HttpStatus;
import io.javalin.openapi.HttpMethod;
import io.javalin.openapi.OpenApi;
import io.javalin.openapi.OpenApiResponse;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;

public class BaseController implements Controller {

    public void routes() {
        path("/ping", () -> get(this::ping));
    }

    @OpenApi(
            summary = "Return a healthcheck message",
            operationId = "",
            path = "/api/v1/ping/",
            methods = HttpMethod.GET,
            tags = {"monitoring"},
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "500"),
            }
    )
    public void ping(Context context) {
        PingResponse response = new PingResponse();
        response.message = "pong";
        context.json(response).status(HttpStatus.OK);
    }

}
