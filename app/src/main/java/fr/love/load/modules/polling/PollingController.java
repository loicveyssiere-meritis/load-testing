package fr.love.load.modules.polling;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.love.load.server.Controller;
import io.javalin.http.Context;
import io.javalin.http.HttpStatus;
import io.javalin.openapi.HttpMethod;
import io.javalin.openapi.OpenApi;
import io.javalin.openapi.OpenApiResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;

public class PollingController implements Controller {

    private static final int POLLING_SIZE = 40;

    public void routes() {
        path("/polling", () -> get(this::polling));
    }

    static public String generateRandomString() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 12;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatedString;
    }

    static public List<PollingResponse> generatePollPayload() {
        ArrayList<PollingResponse> responses = new ArrayList<>();
        for (int i = 0; i < POLLING_SIZE; i++) {
            PollingResponse response = new PollingResponse();
            response.name = generateRandomString();
            response.company = generateRandomString();
            response.description = generateRandomString();
            response.summary = generateRandomString();
            response.key = generateRandomString();
            response.secret = generateRandomString();
            response.info = generateRandomString();
            responses.add(response);
        }
        return responses;
    }

    @OpenApi(
            summary = "Return an interval small payload",
            operationId = "",
            path = "/api/v1/polling/",
            methods = HttpMethod.GET,
            tags = {"static"},
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "500"),
            }
    )
    public void polling(Context context) {
        List<PollingResponse> response = generatePollPayload();
        context.json(response).status(HttpStatus.OK);
    }
}