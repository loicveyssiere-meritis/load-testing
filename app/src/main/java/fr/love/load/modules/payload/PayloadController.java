package fr.love.load.modules.payload;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import fr.love.load.server.Controller;
import io.javalin.http.Context;
import io.javalin.http.HttpStatus;
import io.javalin.openapi.HttpMethod;
import io.javalin.openapi.OpenApi;
import io.javalin.openapi.OpenApiResponse;

import java.io.*;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;

public class PayloadController implements Controller {

    private final String payloadPath = "/payload.json";
    private JsonNode payload;

    public PayloadController() {
        ObjectMapper mapper = new ObjectMapper();
        BufferedReader reader;
        InputStream inputStream = getClass().getResourceAsStream(payloadPath);
        reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            payload = mapper.readTree(reader);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void routes() {
        path("/heavy", () -> get(this::heavy));
    }

    @OpenApi(
            summary = "Return a heavy payload",
            operationId = "",
            path = "/api/v1/heavy/",
            methods = HttpMethod.GET,
            tags = {"static"},
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "500"),
            }
    )
    public void heavy(Context context) {
        context.json(payload).status(HttpStatus.OK);
    }

}
