package fr.love.load.modules.base;

import io.javalin.openapi.OpenApiByFields;

@OpenApiByFields
public class PingResponse {
    public String message;
}
