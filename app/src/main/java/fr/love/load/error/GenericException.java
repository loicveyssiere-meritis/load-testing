package fr.love.load.error;

import io.javalin.http.Context;
import io.javalin.http.ExceptionHandler;
import io.javalin.http.HttpStatus;
import io.javalin.validation.ValidationException;

public class GenericException implements ExceptionHandler<ValidationException> {

    @Override
    public void handle(ValidationException e, Context context) {
        context.status(HttpStatus.BAD_REQUEST).json(e.getErrors());
    }
}
