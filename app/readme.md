# App Server

## With Docker

```bash
cd app/
docker build -t load_image .
docker run --name load_container -p 8080:8080 load_image
```

# Manual Test

```bash
curl http://localhost:8080/api/v1/ping
```
