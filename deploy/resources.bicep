//
// Parameters
//

@description('Location in which all resources will be created')
param location string = resourceGroup().location

@description('App prefix to be used in resources names')
param prefix string = resourceGroup().name

@secure()
@description('Public SSH key of the admin user of the created VM.')
param adminPublicKey string

@description('Domain Name configured in the principal DNS')
param domainName string


var numVM = 2


//
// Variables
//


// TODO : create a deployment account and pass its ID 

//
// Modules
//

// network module
module network 'resources/network.bicep' = {
  name: 'create-network'
  params: {
    prefix: prefix
    location: location
  }
}

// DNS settings
resource appPrivateDNSZone 'Microsoft.Network/privateDnsZones@2018-09-01' = {
  name: 'privatelink.azurewebsites.net'
  location: 'global'
}

resource appDnsLink 'Microsoft.Network/privateDnsZones/virtualNetworkLinks@2018-09-01' = {
  parent: appPrivateDNSZone
  name: 'appservice-private-endpoint-link'
  location: 'global'
  properties: {
    registrationEnabled: false
    virtualNetwork: {
      id: network.outputs.networkID
    }
  }
}

// POOL1
module vm 'resources/vm.bicep' = [for i in range(0, numVM): {
  name: 'create-virtual-machine-${i}'
  params: {
    location: location
    vmSize: 'Standard_B2s'
    vmShortName: 'load-testing-vm-${i}'
    subnetId: network.outputs.backendSubnetAddress1ID
    adminPublicKey: adminPublicKey
  }
}]

// POOL2
module appservice 'resources/app-service.bicep' = {
  name: 'create-service'
  params: {
    appName: 'load-testing-app'
    location: location
    sku: 'S1'
    skuCode: 'Standard'
    linuxFxVersion: 'DOCKER|registry.gitlab.com/loicveyssiere-meritis/load-testing/app:latest'

    inboundSubnetID: network.outputs.backendSubnetAddress2ID
    outboundSubnetID: network.outputs.backendSubnetAddressOut2ID
    privateDnsZoneId: appPrivateDNSZone.id
  }
}

// POOL3
module appcontainer 'resources/app-container.bicep' = {
  name: 'create-appcontainer'
  params: {
    location: location
    appName: 'load-testing-app'
    subnetID: network.outputs.backendSubnetAddress3ID
  }
}

module appcontainerconnect 'resources/app-container-connect.bicep' = {
  name: 'create-appcontainerconnect'
  params: {
    defaultDomain: appcontainer.outputs.defaultDomain
    networkID: network.outputs.networkID
    staticIP: appcontainer.outputs.staticIP
  }
}


// gateway module
module appGateway 'resources/gateway.bicep' = {
  name: 'create-gateway'
  params: {
    prefix: prefix
    location: location

    subnetID: network.outputs.gatewaySubnetID
    domainName: domainName
    pool1: [ for i in range(0, numVM): {
      ipAddress: vm[i].outputs.vmIP
    }]
    pool2: [{
      fqdn: appservice.outputs.appFQDN
    }]
    pool3: [{
      fqdn: '${appcontainer.outputs.appName}.${appcontainer.outputs.defaultDomain}'
    }]
  }
  dependsOn: [
    network
  ]
}

