//
// Parameters
//

@description('Location in which all resources will be created.')
param location string

@description('Size of the created VM.')
param vmSize string

@description('Short name of the created VM.')
param vmShortName string

@description('Name of the admin user of the created VM.')
param adminUsername string = 'azureuser'

@secure()
@description('Public SSH key of the admin user of the created VM.')
param adminPublicKey string


@description('Disk size in GB of the created VM.')
param diskSizeGB int = 30

@description('Id of the subnet of the created VM.')
param subnetId string


//
// Variables
//

var vmUniqueName = '${vmShortName}-${uniqueString(resourceGroup().id)}'

//
// Resources
//
resource publicIP 'Microsoft.Network/publicIPAddresses@2021-03-01' = {
  name: 'publicIP-${vmUniqueName}'
  location: location
  sku: {
    name: 'Basic'
  }
  properties: {
    publicIPAllocationMethod: 'Static'
    publicIPAddressVersion: 'IPv4'
    dnsSettings: {
      domainNameLabel: vmShortName
    }
  }
}

resource securityGroup 'Microsoft.Network/networkSecurityGroups@2021-03-01' = {
  name: 'securityGroup-${vmUniqueName}'
  location: location
  properties: {
    securityRules: [
      {
        name: 'allow-inbound-tcp-on-port-22'
        properties: {
          access: 'Allow'
          direction: 'Inbound'
          protocol: 'Tcp'
          sourceAddressPrefix: '*'
          sourcePortRange: '*'
          destinationAddressPrefix: '*' // publicIP.properties.ipAddress
          destinationPortRange: '22'
          priority: 100
        }
      }
      {
        name: 'allow-inbound-tcp-on-port-80'
        properties: {
          access: 'Allow'
          direction: 'Inbound'
          protocol: 'Tcp'
          sourceAddressPrefix: '*'
          sourcePortRange: '*'
          destinationAddressPrefix: '*' // publicIP.properties.ipAddress
          destinationPortRange: '80'
          priority: 200
        }
      }
    ]
  }
}

resource networkInterface 'Microsoft.Network/networkInterfaces@2021-03-01' = {
  name: 'networkInterface-${vmUniqueName}'
  location: location
  properties: {
    ipConfigurations: [
      {
        name: 'ipconfig'
        properties: {
          publicIPAddress: {
            id: publicIP.id
          }
          subnet: {
            id: subnetId
          }
        }
      }
    ]
    networkSecurityGroup: {
      id: securityGroup.id
      location: location
    }
  }
}

resource virtualMachine 'Microsoft.Compute/virtualMachines@2021-07-01' = {
  name: vmShortName
  location: location
  identity: {
    type: 'SystemAssigned'
  }
  properties: {
    hardwareProfile: {
      vmSize: vmSize
    }
    osProfile: {
      computerName: vmShortName
      adminUsername: adminUsername
      customData: loadFileAsBase64('cloud-init.yml')
      linuxConfiguration: {
        disablePasswordAuthentication: true
        ssh: {
          publicKeys: [
            {
              path: '/home/${adminUsername}/.ssh/authorized_keys'
              keyData: adminPublicKey
            }
          ]
        }

        provisionVMAgent: true
        patchSettings: {
          patchMode: 'AutomaticByPlatform'
        }
      }
    }
    storageProfile: {
      imageReference: {
        publisher: 'Canonical'
        offer: '0001-com-ubuntu-server-focal'
        sku: '20_04-lts'
        version: 'latest'
      }
      osDisk: {
        createOption: 'FromImage'
        diskSizeGB: diskSizeGB
      }
    }
    networkProfile: {
      networkInterfaces: [
        {
          id: networkInterface.id
        }
      ]
    }
  }

  resource sshExtension 'extensions' = {
    name: 'sshExtension-${vmShortName}'
    location: location
    properties: {
      publisher: 'Microsoft.Azure.ActiveDirectory'
      type: 'AADSSHLoginForLinux'
      typeHandlerVersion: '1.0'
      autoUpgradeMinorVersion: true
    }
  }

  resource scriptExtension 'extensions' = {
    name: 'scriptExtension-${vmShortName}'
    location: location
    properties: {
      publisher: 'Microsoft.Azure.Extensions'
      type: 'CustomScript'
      typeHandlerVersion: '2.1'
      autoUpgradeMinorVersion: true
      protectedSettings: {
        script: loadFileAsBase64('update.sh')
      }
    }
  }
}

//
// Outputs
//

output virtualMachineId string = virtualMachine.id
output vmIP string = networkInterface.properties.ipConfigurations[0].properties.privateIPAddress
output virtualMachineBaseName string = vmShortName
