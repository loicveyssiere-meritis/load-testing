//
// Parameters
//

@description('Location in which all resources will be created.')
param location string = resourceGroup().location

@description('App prefix to be used in resources names')
param prefix string = resourceGroup().name

@description('VPC CIDR')
param vnetAddress string                    = '10.6.0.0/16'

@description('Gateway subnet CIDR')
param gatewaySubnetAddress string           = '10.6.0.0/24'

@description('Backend subnet 1 CIDR')
param backendSubnetAddress1 string    = '10.6.1.0/24'
@description('Backend subnet 2 CIDR')
param backendSubnetAddress2 string   = '10.6.2.0/24'
@description('Backend subnet 3 CIDR')
param backendSubnetAddress3 string   = '10.6.10.0/23'

@description('Backend subnet 3 CIDR')
param backendSubnetAddressOut2 string   = '10.6.4.0/24'

//
// Resources
//

// VPC with all component hosting subnets 
resource vnet 'Microsoft.Network/virtualNetworks@2022-05-01' = {
  name: '${prefix}-vnet'
  location: location
  properties: {
    addressSpace: {
      addressPrefixes: [
        vnetAddress
      ]
    }
    subnets: [  
      // public subnet for the gateway
      {
        type: 'Microsoft.Network/virtualNetworks/subnets'
        name: '${prefix}-gatewaySubnet'
        properties: {
          addressPrefix: gatewaySubnetAddress
          networkSecurityGroup:{
            id: gwNsg.id
          }
        }
      }
      {
        type: 'Microsoft.Network/virtualNetworks/subnets'
        name: '${prefix}-backendSubnet1'
        properties: {
          addressPrefix: backendSubnetAddress1
          delegations: []

        }
      }
      {
        type: 'Microsoft.Network/virtualNetworks/subnets'
        name: '${prefix}-backendSubnet2'
        properties: {
          addressPrefix: backendSubnetAddress2
          delegations: []
        }
      }
      {
        type: 'Microsoft.Network/virtualNetworks/subnets'
        name: '${prefix}-backendSubnet3'
        properties: {
          addressPrefix: backendSubnetAddress3
          delegations: []
        }
      }
      {
        type: 'Microsoft.Network/virtualNetworks/subnets'
        name: '${prefix}-backendSubnetOut2'
        properties: {
          addressPrefix: backendSubnetAddressOut2
          delegations: [
            {
              name: 'delegation'
              properties: {
                serviceName: 'Microsoft.Web/serverFarms'
              }
            }
          ]
        }
      }
    ]
    virtualNetworkPeerings: []
    enableDdosProtection: false
  }
}

// Security Groups for apps
// allowing http/s traffic
resource appNsg 'Microsoft.Network/networkSecurityGroups@2016-03-30' = {
  name: '${prefix}-app-nsg'
  location: location
  properties: {
    securityRules: [
      {
        name: 'Allow22'
        properties: {
          description: 'Allow 22 from local VNet'
          protocol: 'Tcp'
          sourcePortRange: '*'
          destinationPortRange: '22'
          sourceAddressPrefix: 'VirtualNetwork'
          destinationAddressPrefix: '*'
          access: 'Allow'
          priority: 100
          direction: 'Inbound'
        }
      }
      {
        name: 'Allow80'
        properties: {
          description: 'Allow 80 from local VNet'
          protocol: 'Tcp'
          sourcePortRange: '*'
          destinationPortRange: '80'
          sourceAddressPrefix: 'VirtualNetwork'
          destinationAddressPrefix: '*'
          access: 'Allow'
          priority: 101
          direction: 'Inbound'
        }
      }
      {
        name: 'Allow443'
        properties: {
          description: 'Allow 443 from local VNet'
          protocol: 'Tcp'
          sourcePortRange: '*'
          destinationPortRange: '443'
          sourceAddressPrefix: 'VirtualNetwork'
          destinationAddressPrefix: '*'
          access: 'Allow'
          priority: 102
          direction: 'Inbound'
        }
      }
    ]
  }
}

// Security group for the application gateway 
// accepting http/s traffic
resource gwNsg 'Microsoft.Network/networkSecurityGroups@2016-03-30' = {
  name: '${prefix}-gtw-nsg'
  location: location
  properties: {
    securityRules: [
      {
        name: 'Allow80'
        properties: {
          description: 'Allow 80 from Internet'
          protocol: 'Tcp'
          sourcePortRange: '*'
          destinationPortRange: '80'
          sourceAddressPrefix: 'Internet'
          destinationAddressPrefix: '*'
          access: 'Allow'
          priority: 100
          direction: 'Inbound'
        }
      }
      {
        name: 'Allow443'
        properties: {
          description: 'Allow 443 from Internet'
          protocol: 'Tcp'
          sourcePortRange: '*'
          destinationPortRange: '443'
          sourceAddressPrefix: 'Internet'
          destinationAddressPrefix: '*'
          access: 'Allow'
          priority: 102
          direction: 'Inbound'
        }
      }
      {
        name: 'Allow_65200_65535'
        properties: {
          description: 'Allow 65200 - 65535'
          protocol: 'Tcp'
          sourcePortRange: '*'
          destinationPortRange: '65200 - 65535'
          sourceAddressPrefix: 'Internet'
          destinationAddressPrefix: '*'
          access: 'Allow'
          priority: 103
          direction: 'Inbound'
        }
      }
    ]
  }
}

//
// Outputs
//
output networkID string                 = vnet.id

output gatewaySubnetID string           = vnet.properties.subnets[0].id

output backendSubnetAddress1ID string   = vnet.properties.subnets[1].id
output backendSubnetAddress2ID string   = vnet.properties.subnets[2].id
output backendSubnetAddress3ID string   = vnet.properties.subnets[3].id
output backendSubnetAddressOut2ID string   = vnet.properties.subnets[4].id
