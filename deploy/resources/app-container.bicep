@description('Location in which all resources will be created')
param location string = resourceGroup().location

@description('Subnet ID in which the app private link will be created')
param subnetID string 

@description('App name for resource naming')
param appName string 

var appContainerName = '${appName}-containerapp'
var appContainerEnvName = '${appName}-cae'


resource appContainerEnv 'Microsoft.App/managedEnvironments@2023-05-01' = {
  name: appContainerEnvName
  location: location
  properties: {
    appLogsConfiguration: {
      destination: 'azure-monitor'
      logAnalyticsConfiguration: null
    }
    vnetConfiguration: {
      infrastructureSubnetId: subnetID
      internal: true
    }
    zoneRedundant: false
  }
}

resource appContainer 'Microsoft.App/containerApps@2023-05-01' = {
  name: appContainerName
  location: location

  properties: {
    configuration: {
      ingress: {
        allowInsecure: true
        external: true
        targetPort: 8080
      }
      registries: []
      secrets: []
    }
    environmentId: appContainerEnv.id
    template: {
      containers: [
        {
          name: 'app-container'
          image: 'registry.gitlab.com/loicveyssiere-meritis/load-testing/app'
          resources: {
            cpu: '0.25'
            memory: '.5Gi'
          }
        }
      ]
      scale: {
        minReplicas: 2
        maxReplicas: 2
        rules: null
      }
    }
  }
}

output defaultDomain string = appContainerEnv.properties.defaultDomain
output staticIP string = appContainerEnv.properties.staticIp
output appName string = appContainerName
