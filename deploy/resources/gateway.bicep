//
// Parameters
//

@description('Location in which all resources will be created')
param location string = resourceGroup().location

@description('App prefix to be used in resources names')
param prefix string = '${location}-load'

@description('Subnet ID to which the application gateway will be integrated')
param subnetID string 

@description('Domain Name configured in the principal DNS')
param domainName string

param pool1 array
param pool2 array
param pool3 array

//
// variables for multi referencing
//

var applicationGateWayName = '${prefix}-gtw'
var publicIPAddressName = '${prefix}-pip'

//
// Resources
//


// the public ip to be used by the app gateway 
resource public_ip 'Microsoft.Network/publicIPAddresses@2022-05-01' = {
  name: publicIPAddressName
  location: location
  sku: {
    name: 'Standard'
    tier: 'Regional'
  }
  properties: {
    publicIPAddressVersion: 'IPv4'
    publicIPAllocationMethod: 'Static'
    idleTimeoutInMinutes: 4
    ipTags: []
  }
}

// the application gateway
resource appGateway 'Microsoft.Network/applicationGateways@2022-05-01' = {
  name: applicationGateWayName
  location: location
  properties: {

    sku: {
      name: 'Standard_v2'
      tier: 'Standard_v2'
      capacity: 2
    }

    // subnet to which app gateway will integrate
    gatewayIPConfigurations: [
      {
        name: 'appGatewayIpConfig'
        properties: {
          subnet: {
            id: subnetID
          }
        }
      }
    ]

    // ssl certificate to be used by Https listner for traffic encryption
    sslCertificates: []
    trustedRootCertificates: []
    trustedClientCertificates: []
    sslProfiles: []

    // frontend public IP reference
    frontendIPConfigurations: [
      {
        name: 'appGwPublicFrontendIp'
        properties: {
          privateIPAllocationMethod: 'Dynamic'
          publicIPAddress: {
            id: public_ip.id
          }
        }
      }
    ]

    // declaration of ports to be used by Http/s listeners
    frontendPorts: [
      {
        // TODO : to be removed 
        name: 'port_80'
        properties: {
          port: 80
        }
      }
    ]

    // target backend FQDN 
    backendAddressPools: [
      {
        name: 'appGatewayBackpool1'
        properties: {
          backendAddresses: [for item in pool1: item]
        }
      }
      {
        name: 'appGatewayBackpool2'
        properties: {
          backendAddresses: [for item in pool2: item]
        }
      }
      {
        name: 'appGatewayBackpool3'
        properties: {
          backendAddresses: [for item in pool3: item]
        }
      }
    ]
    loadDistributionPolicies: []
    backendSettingsCollection: []

    // target backend configuration
    backendHttpSettingsCollection: [
      {
        name: 'appGatewayBackSettings1'
        properties: {
          port: 80
          protocol: 'Http'
          cookieBasedAffinity: 'Disabled'
          pickHostNameFromBackendAddress: false
          requestTimeout: 60
          probe: {
            id: resourceId('Microsoft.Network/applicationGateways/probes', applicationGateWayName, 'test-backend-health1')
          }
        }
      }
      {
        name: 'appGatewayBackSettings2'
        properties: {
          port: 80
          protocol: 'Http'
          cookieBasedAffinity: 'Disabled'
          pickHostNameFromBackendAddress: true
          requestTimeout: 60
          probe: {
            id: resourceId('Microsoft.Network/applicationGateways/probes', applicationGateWayName, 'test-backend-health2')
          }
        }
      }
      {
        name: 'appGatewayBackSettings3'
        properties: {
          port: 80
          protocol: 'Http'
          cookieBasedAffinity: 'Disabled'
          pickHostNameFromBackendAddress: true
          requestTimeout: 60
          probe: {
            id: resourceId('Microsoft.Network/applicationGateways/probes', applicationGateWayName, 'test-backend-health3')
          }
        }
      }
    ]

    // http/https traffic listeners
    httpListeners: [
      // POOL1 
      {
        name: 'appGatewayHttpListener1'
        properties: {
          protocol: 'Http'
          hostName: 'app1.${domainName}'
          requireServerNameIndication: false
          customErrorConfigurations: []
          frontendPort: {
            // port reference
            id: resourceId('Microsoft.Network/applicationGateways/frontendPorts', applicationGateWayName, 'port_80')
          }
          frontendIPConfiguration: {
            // public ip reference
            id: resourceId('Microsoft.Network/applicationGateways/frontendIPConfigurations', applicationGateWayName, 'appGwPublicFrontendIp')
          }
        }
      }
      // POOL2 
      {
        name: 'appGatewayHttpListener2'
        properties: {
          protocol: 'Http'
          hostName: 'app2.${domainName}'
          requireServerNameIndication: false
          customErrorConfigurations: []
          frontendPort: {
            // port reference
            id: resourceId('Microsoft.Network/applicationGateways/frontendPorts', applicationGateWayName, 'port_80')
          }
          frontendIPConfiguration: {
            // public ip reference
            id: resourceId('Microsoft.Network/applicationGateways/frontendIPConfigurations', applicationGateWayName, 'appGwPublicFrontendIp')
          }
        }
      }
      // POOL3 
      {
        name: 'appGatewayHttpListener3'
        properties: {
          protocol: 'Http'
          hostName: 'app3.${domainName}'
          requireServerNameIndication: false
          customErrorConfigurations: []
          frontendPort: {
            // port reference
            id: resourceId('Microsoft.Network/applicationGateways/frontendPorts', applicationGateWayName, 'port_80')
          }
          frontendIPConfiguration: {
            // public ip reference
            id: resourceId('Microsoft.Network/applicationGateways/frontendIPConfigurations', applicationGateWayName, 'appGwPublicFrontendIp')
          }
        }
      }
    ]
    listeners: []
    urlPathMaps: []
    
    // gateway routing configuration
    requestRoutingRules: [
      {
        name: 'rule1'
        properties: {
          ruleType: 'Basic'
          priority: 1
          httpListener: {
            id: resourceId('Microsoft.Network/applicationGateways/httpListeners', applicationGateWayName, 'appGatewayHttpListener1')
          }
          backendAddressPool: {
            id: resourceId('Microsoft.Network/applicationGateways/backendAddressPools', applicationGateWayName, 'appGatewayBackpool1')
          }
          backendHttpSettings: {
            id: resourceId('Microsoft.Network/applicationGateways/backendHttpSettingsCollection', applicationGateWayName, 'appGatewayBackSettings1')
          }
        }
      }
      {
        name: 'rule2'
        properties: {
          ruleType: 'Basic'
          priority: 2
          httpListener: {
            id: resourceId('Microsoft.Network/applicationGateways/httpListeners', applicationGateWayName, 'appGatewayHttpListener2')
          }
          backendAddressPool: {
            id: resourceId('Microsoft.Network/applicationGateways/backendAddressPools', applicationGateWayName, 'appGatewayBackpool2')
          }
          backendHttpSettings: {
            id: resourceId('Microsoft.Network/applicationGateways/backendHttpSettingsCollection', applicationGateWayName, 'appGatewayBackSettings2')
          }
        }
      }
      {
        name: 'rule3'
        properties: {
          ruleType: 'Basic'
          priority: 3
          httpListener: {
            id: resourceId('Microsoft.Network/applicationGateways/httpListeners', applicationGateWayName, 'appGatewayHttpListener3')
          }
          backendAddressPool: {
            id: resourceId('Microsoft.Network/applicationGateways/backendAddressPools', applicationGateWayName, 'appGatewayBackpool3')
          }
          backendHttpSettings: {
            id: resourceId('Microsoft.Network/applicationGateways/backendHttpSettingsCollection', applicationGateWayName, 'appGatewayBackSettings3')
          }
        }
      }
    ]
    routingRules: []
    probes: [
      {
        name: 'test-backend-health1'
        properties: {
          protocol: 'Http'
          path: '/api/v1/ping'
          interval: 30
          timeout: 30
          unhealthyThreshold: 3
          pickHostNameFromBackendHttpSettings: false
          host: '127.0.0.1'
          minServers: 0
          match: {
            statusCodes: ['200']
          }
        }
      }
      {
        name: 'test-backend-health2'
        properties: {
          protocol: 'Http'
          path: '/api/v1/ping'
          interval: 30
          timeout: 30
          unhealthyThreshold: 3
          pickHostNameFromBackendHttpSettings: true
          minServers: 0
          match: {
            statusCodes: ['200']
          }
        }
      }
      {
        name: 'test-backend-health3'
        properties: {
          protocol: 'Http'
          path: '/api/v1/ping'
          interval: 30
          timeout: 30
          unhealthyThreshold: 3
          pickHostNameFromBackendHttpSettings: true
          minServers: 0
          match: {
            statusCodes: ['200']
          }
        }
      }
    ]
    rewriteRuleSets: []
    redirectConfigurations: []
    privateLinkConfigurations: []
    enableHttp2: false

  }
}

//
// Outputs
//

output publicIPID string = public_ip.id
output appGtwFQDN string = '${prefix}-gtw.francecentral.cloudapp.azure.com'
