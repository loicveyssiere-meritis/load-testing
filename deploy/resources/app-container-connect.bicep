
@description('')
param defaultDomain string 

@description('')
param networkID string 

@description('')
param staticIP string 


resource appPrivateDNSZone2 'Microsoft.Network/privateDnsZones@2018-09-01' = {
  name: defaultDomain
  location: 'global'
}

resource appDnsLink2 'Microsoft.Network/privateDnsZones/virtualNetworkLinks@2018-09-01' = {
  parent: appPrivateDNSZone2
  name: 'appservice-private-endpoint-link2'
  location: 'global'
  properties: {
    registrationEnabled: false
    virtualNetwork: {
      id: networkID
    }
  }
}

resource record 'Microsoft.Network/privateDnsZones/A@2018-09-01' = {
  parent: appPrivateDNSZone2
  name: '*'
  properties: {
    ttl: 3600
    aRecords: [
      {
        ipv4Address: staticIP
      }
    ]
  }
}
