//
// Parameters
//

@description('Location in which all resources will be created')
param location string = resourceGroup().location

@description('DNS private zone ID in which app url will point to private ip')
param privateDnsZoneId string

@description('Subnet ID in which the app private link will be created')
param inboundSubnetID string 
@description('Subnet ID to which the app will be integrated')
param outboundSubnetID string

@description('Instance hosting service sku')
param sku string =  'S1'
@description('Instance hosting service skuCode')
param skuCode string = 'Standard'

@description('App name for resource naming')
param appName string 
@description('App container image uri to be deployed')
param linuxFxVersion string = 'DOCKER|mcr.microsoft.com/appsvc/staticsite:latest'

//
// Variables
//

// TODO : trim location ? 'East US' for ex
var appServiceName = '${appName}-appservice'
var appServicePlanName = '${appName}-asp'

//
// Resources
//

// Machine plan hosting the app service
resource appServicePlan 'Microsoft.Web/serverfarms@2022-03-01' = {
  name: appServicePlanName
  location: location
  kind: 'linux'
  properties: {
    reserved: true
  }
  sku: {
    name: sku
    tier: skuCode
  }
  dependsOn: []
  tags: {
  }
}

// App service resource 
resource appService 'Microsoft.Web/sites@2022-03-01' = {
  name: appServiceName
  location: location
  kind: 'app,linux,container'
  tags: {}
  properties: {
    siteConfig: {
      // TODO : docker configs not needed if good principal used ?
      appSettings:[
        {
          name: 'JAVALIN_PORT'
          value: '80'
        }
      ]
      linuxFxVersion: linuxFxVersion
      numberOfWorkers: 1
      alwaysOn: true
    }
    serverFarmId: appServicePlan.id
    virtualNetworkSubnetId: outboundSubnetID
  }
  
}

// App service private endpoint to be hosted in a private subnet
resource appServicePrivateEndpoint 'Microsoft.Network/privateEndpoints@2022-05-01' = {
  name: '${appServiceName}-private-endpoint'
  location: location
  properties: {
    subnet: {
      id: inboundSubnetID
    }
    privateLinkServiceConnections: [
      {
        name: '${appServiceName}-private-link-connection'
        properties: {
          privateLinkServiceId: appService.id
          groupIds: [
            'sites'
          ]
        }
      }
    ]
  }
}

resource appservicePrivateEndpointDnsgroupname 'Microsoft.Network/privateEndpoints/privateDnsZoneGroups@2022-05-01' = {
  parent: appServicePrivateEndpoint
  name: '${appServiceName}-private-endpoint-dnsgroupname'
  properties: {
    privateDnsZoneConfigs: [
      {
        name: '${appServiceName}-private-link-conf'
        properties: {
          privateDnsZoneId: privateDnsZoneId
        }
      }
    ]
  }
}

//
// Outputs
//

output appFQDN string = '${appServiceName}.azurewebsites.net'
