#!/bin/sh

#
# Dependencies
#

# Install Ctop. See: https://github.com/bcicen/ctop#linux-generic
sudo wget https://github.com/bcicen/ctop/releases/download/0.7.6/ctop-0.7.6-linux-amd64 -O /usr/local/bin/ctop
sudo chmod +x /usr/local/bin/ctop

# Install the dependencies for which a package is available.
sudo apt update
sudo apt install -y \
    fail2ban \
    python2 \
    silversearcher-ag \
    tmux \
    tree

#
# Configuration for all VMs
#

# Install a cronjob to remove stopped containers in order to save disk space.
sudo tee /etc/cron.daily/remove-stopped-docker-containers <<EOF
#!/usr/bin/env bash
# Remove all stopped Docker containers at the end of the day.
docker rm \$(docker ps --all --quiet)
EOF
sudo chmod +x /etc/cron.daily/remove-stopped-docker-containers

# Install a cronjob to remove unused images in order to save disk space.
sudo tee /etc/cron.daily/remove-unused-docker-images <<EOF
#!/usr/bin/env bash
# Remove all unused Docker images at the end of the day.
docker rmi \$(docker images --all --quiet)
EOF
sudo chmod +x /etc/cron.daily/remove-unused-docker-images

# Configure fail2ban.
sudo tee /etc/fail2ban/jail.d/sshd.conf <<EOF
[sshd]
enabled = true
mode = aggressive
logpath = /var/log/auth.log
filter = sshd
port = ssh
maxretry = 3
bantime = 600s
EOF
sudo service fail2ban restart

# Alias "python" to run Python 2, as required by the LinuxDiagnostic extension. For more information, see:
# https://docs.microsoft.com/en-us/azure/virtual-machines/extensions/diagnostics-linux#python-requirement
sudo update-alternatives --remove-all python
sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 1

#
# RUN 
#
date
sleep 60 # hack wait for cloud-init to finish
date

export JAVALIN_PORT=8080
docker rm app > /dev/null
docker run --name app -d -p 80:8080 registry.gitlab.com/loicveyssiere-meritis/load-testing/app:latest
docker ps -a

echo "Update done!"